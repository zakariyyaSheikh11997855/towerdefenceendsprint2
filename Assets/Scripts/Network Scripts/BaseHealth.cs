﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

//namespace AssemblyCSharp
//{
	public class BaseHealth : NetworkBehaviour
	{
		[SerializeField]
		public int maxHealth; 

		[SyncVar] 
		public int currentHealth; 

		public BaseHealth ()
		{
			//Each inheerting class should set the 
		}

		public void Start()
		{
			//Each inheerting class should set the Max health and set the current health like so
			//currentHealth = maxHealth; [Player does it slightly differently]
		}

		[ClientRpc] //Client will call server and tell server copy to take the damage 
		public void RpcTakeDamage(int _amount)
		{
			//Override this for more complex actions
			currentHealth -= _amount;

			if(currentHealth <= 0)
			{
				die();
			}
		}

		[ClientRpc] 
		public void RpcHealDamage(int _amount)
		{
			//Override this for more complex actions
			currentHealth = Mathf.Min(currentHealth + _amount, maxHealth);
		}


		private void die()
		{
			//TODO: determine if I can leave the defaul die inside this class
			Destroy(gameObject);
		}

	}
//}

